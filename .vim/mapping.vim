" KEYBINDINGS

" Save File
inoremap <Space><Space> <Esc>:w<Enter>i

" Save and Exit
inoremap ;sq <Esc>:w<Enter>:q<Enter>
