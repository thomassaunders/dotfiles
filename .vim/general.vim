" Tabs to Spaces
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4

" Line Numbers
set number

" Use previous indent
set autoindent

" Set command are to 2 lines
set cmdheight=2
